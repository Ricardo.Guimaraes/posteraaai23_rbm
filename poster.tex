% !TeX program = lualatex
\documentclass[UKenglish,xcolor=table,20pt,final]{beamer}

\usepackage[utf8]{luainputenc}
\usepackage{fontspec-luatex}
\usepackage{fontawesome5}

\usepackage[size=custom, width=76.2, height=101.6, orientation=portrait, scale=1]{beamerposter}
\usetheme{gemini}
\usecolortheme{mit}

\usepackage{xparse}
\usepackage{tikz}
\usepackage{marvosym}
\usepackage{tikzsymbols}
\usepackage{smartdiagram}
\usepackage{pgfplots}
\pgfplotsset{compat=1.18}
\usepgfplotslibrary{fillbetween}
\usepackage{ragged2e}
\usepackage{calc}
\usepackage{forest}
\usepackage{tikz-qtree}
\usetikzlibrary{calc, positioning, shapes, matrix,
    decorations,fit,intersections,patterns,arrows,shapes.arrows,
arrows.meta,trees,shapes.callouts, trees,shapes.symbols, spy}
\usetikzlibrary{graphs,graphdrawing,quotes,graphs.standard}
\usesmartdiagramlibrary{additions}
\usegdlibrary{force}
\usegdlibrary{layered}
\usegdlibrary{trees}
\usegdlibrary{circular}
\usetikzlibrary{mindmap}

\usepackage[draft]{tikzpeople}

\usepackage{booktabs}
\usepackage{mathtools}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{hyperref}
% \usepackage{cleveref}
\usepackage{anyfontsize}

%\usepackage{siunitx} % automatic formatting of large numbers

\newtheorem{proposition}[theorem]{Proposition}

\usepackage{multirow}
\usepackage{xcolor}
\usepackage{appendixnumberbeamer}
\usepackage{colortbl}
\usepackage[normalem]{ulem} % strikeout text and the like
\usepackage{epstopdf} % Use eps images with Lualatex (for the logos)
% Break and typset long URLs:
%\usepackage[anythingbreaks]{breakurl}
\usepackage{xurl}
\usepackage{setspace}

%\usepackage{bussproofs} % for typesetting proof trees

\usepackage[
backend=biber,
bibencoding=utf8,
style=alphabetic
]{biblatex}
\addbibresource{poster.bib}

\usepackage{annotate-equations}

\definecolor{oiorange}{HTML}{E69F00}
\definecolor{oiskyblue}{HTML}{56B4E9}
\definecolor{oibluishgreen}{HTML}{009E73}
\definecolor{oiyellow}{HTML}{F0E442}
\definecolor{oiblue}{HTML}{0072B2}
\definecolor{oivermillion}{HTML}{D55E00}
\definecolor{oireddishpurple}{HTML}{CC79A7}

\definecolor{colorAO}{rgb}{0.0, 0.5, 0.0}
\definecolor{amber}{rgb}{1.0, 0.75, 0.0}
\definecolor{amethyst}{rgb}{0.6, 0.4, 0.8}
\definecolor{cherryblossompink}{rgb}{1.0, 0.72, 0.77}
\definecolor{darkelectricblue}{rgb}{0.33, 0.41, 0.47}
\definecolor{bittersweet}{rgb}{1.0, 0.44, 0.37}

\colorlet{mycolor1}{oiblue}
\colorlet{mycolor2}{oiorange}
\colorlet{mycolor3}{oibluishgreen}
\colorlet{mycolor4}{oivermillion}
\colorlet{mycolor5}{oireddishpurple}
\colorlet{mycolor6}{oiskyblue}
\colorlet{mycolor7}{oiyellow}

\colorlet{FRcolor}{oibluishgreen}

\renewcommand{\eqnhighlightshade}{47}

\newcommand{\posmark}{\textcolor{colorAO}{$+$}}
\newcommand{\negmark}{\textcolor{red}{$-$}}
\newcommand{\mylink}[1]{{\textcolor{blue}{\underline{#1}}}}

\newcommand{\thefontsize}{The current font size is: \f@size pt}


\graphicspath{{./images/}}

\setbeamertemplate{caption}

% ====================
% Lengths
% ====================

% If you have N columns, choose \sepwidth and \colwidth such that
% (N+1)*\sepwidth + N*\colwidth = \paperwidth
\newlength{\sepwidth}
\newlength{\colwidth}
\setlength{\sepwidth}{0.03\paperwidth}
\setlength{\colwidth}{0.455\paperwidth}

\newcommand{\separatorcolumn}{\begin{column}{\sepwidth}\end{column}}

\newcommand{\newauthor}[2]{
    \parbox{0.24\textwidth}{
        \texorpdfstring{
            \centering
            #1 \\
            {\small{\urlstyle{same}\url{#2}\urlstyle{tt}}}
        }
        {#1}
    }
}

\setlength\abovecaptionskip{-0.75cm}

% ====================
% Title
% ====================

\title{Finite Based Contraction and Expansion via Models}

% \author{Ricardo Guimarães \inst{1} \and Ana Ozaki \inst{1} \and Jandson S. Ribeiro \inst{2}}
\author{%
  \newauthor{Ricardo Guimarães\inst{1}}{ricardo.guimaraes@uib.no}
\and
  \newauthor{Ana Ozaki\inst{1}}{ana.ozaki@uib.no}
\and
  \newauthor{Jandson S. Ribeiro\inst{2}}{jandson.ribeiro@fernuni-hagen.de}
}

\institute[UiB and FernUni-Hagen]{\inst{1} University of Bergen \samelineand{} \inst{2} University of Hagen}

% ====================
% Footer (optional)
% ====================

\footercontent{%
Part of this work has been done in the context of CEDAS (Center for Data
Science, University of Bergen, Norway). 
The first author is supported by the ERC project ``Lossy Preprocessing''
(LOPRE), grant number 819416, led by\\ Prof Saket Saurabh.
The second author is supported by the NFR project ``Learning Description Logic
Ontologies'', grant number 316022.
The third author is supported by the 
German Research Association (DFG), project number 424710479.}
% (can be left out to remove footer)

% ====================
% Logo (optional)
% ====================

% use this to include logos on the left and/or right side of the header:
\logoleft{{\includegraphics[height=6cm, width=6cm]{images/UiB-emblem_gray.pdf}}}
\logoright{{\includegraphics[height=6cm,width=6cm]{images/feuhagen.png}}}

\input{commands.tex}

\begin{document}

    \begin{frame}[t]
        \begin{columns}[t]
            \separatorcolumn{}

            \begin{column}{\colwidth}
                \begin{block}{1. Motivation}
                    \begin{columns}[c]
                        \column{0.3\textwidth} 
                        \begin{figure}
                            \begin{center}
                                \includegraphics[width=0.7\textwidth]{curiosity_hover.jpg}
                            \end{center}
                            \caption*{\begin{spacing}{0.5}
                                    \scriptsize{Curiosity Rover's Self Portrait at `John Klein' Drilling Site. NASA, 2013, CC BY 2.0}
                            \end{spacing}}
                        \end{figure}
                        \column{0.65\textwidth} 

                        \begin{center}
                            \begin{tikzpicture}

                                \tikzset{kb/.style={line width=1mm, draw, minimum width=5cm, minimum height=2cm, align=center, ellipse, fill=none,inner sep=2pt}}
                                \tikzset{arr/.style={-latex,line width=3mm,font=\itshape, shorten >=4pt,shorten <=4pt}}

                                \node[kb] (orig) at (0, 0) {Original KB};
                                \node[kb, right = 5cm of orig] (new) {New KB};

                                \draw[->, arr] (orig.east) to[] node[midway, above] {New Information} (new.west);
                                \draw[->, arr, dotted, gray] (orig.south east) to[out=-30, bend right, looseness=1.5] node[midway, below] {Minimal Change} (new.south west);

                            \end{tikzpicture}
                        \end{center}
                        \begin{description}
                            \item[Belief Change~\cite{Alchourron1985,Hansson1999}:] Rational change
                            \item[Constraint:] Finite storage/terminating algorithms
                            \item[Flexibility:] Form of new information independent from the KB's language
                        \end{description}
                    \end{columns}
                \end{block}
            \end{column}

            \separatorcolumn{}

            \begin{column}{\colwidth}
                \begin{block}{2. Our Proposal}
                    \vspace{-0.5cm}
                    \begin{center}
                        \begin{tikzpicture}
                            \tikzset{kb/.style={line width=1mm, draw, minimum width=5cm, minimum height=2cm, align=center, ellipse, fill=none,inner sep=2pt}}
                            \tikzset{arr/.style={-latex,line width=3mm,font=\itshape, shorten >=4pt,shorten <=4pt}}

                            \node[kb] (orig) at (0, 0) {Original KB};
                            \node[kb, right = 5cm of orig] (new) {New KB};

                            \draw
                                let
                                \p1=($(orig.west)!0.5!(new.east)$),
                                in
                                node[oibluishgreen] (fr) at (\x1, 2.5) {Finite Bases};

                            \draw[->, arr] (orig.east) to[] node[midway, below] {\textcolor{oiblue}{Model(s)}} (new.west);
                            \draw[->, arr, dotted, gray] (orig.south east) to[out=-30, bend right, looseness=1.5] node[midway, below] {Minimal Change} (new.south west);
                            \draw[->, arr, dashed, oiblue] (fr.south) to[] (orig.north east);
                            \draw[->, arr, dashed, oiblue] (fr.south) to[] (new.north west);

                        \end{tikzpicture}
                    \end{center}

                    \begin{center}
                        \begin{tikzpicture}
                            \tikzset{op/.style={line width=1mm, minimum width=5cm, minimum height=1.25cm, align=center, rounded rectangle, fill=oiblue!20,inner sep=2pt}}
                            \tikzset{lin/.style={line width=1mm,font=\itshape, dashed}}

                            \def\lx{6cm};
                            \def\ly{1cm};
                            \node[op, draw=black] (exp) at (0, 0) {Expansion};
                            \node[op, draw=black, below=\ly of exp] (con) {Contraction};
                            % \node[op, below=\ly of exp] (rev){Revision};


                            \node[op, right=\lx of con, draw=oibluishgreen] (rcp) {Reception};
                            \node[op, draw=oibluishgreen] (evc) at (exp -| rcp) {Eviction};
                            % \node[op, below=\ly of evc, red!10, dotted] (mrev) {\textcolor{black}{?}};

                            \draw[lin, ->] (con) to[] node[midway, align=center] {More models\\Fewer formulas} (rcp);
                            \draw[lin, ->] (exp) to[] node[midway, align=center] {Fewer models\\More formulas} (evc);
                        \end{tikzpicture}
                    \end{center}
                \end{block}
            \end{column}

            \separatorcolumn{}

        \end{columns}

        \begin{columns}[t]
            
            \separatorcolumn{}

            \begin{column}{\colwidth}
                \begin{block}{3. Satisfaction Systems}

                    \vspace{0.5cm}

                    \begin{equation*}
                        \eqnmark[mycolor1]{node1}{\logsys} = (
                        \eqnmarkbox[mycolor2]{node2}{\llang}, 
                        \eqnmarkbox[mycolor3]{node3}{\mUni},
                        \eqnmarkbox[mycolor4]{node4}{\models}
                        )
                    \end{equation*}
                    \annotate[]{below, left}{node1}{\textcolor{black}{Satisfaction System}}
                    \annotate[yshift=1em]{above, left}{node2}{\textcolor{black}{Language}}
                    \annotate[yshift=1em]{above, right}{node3}{\textcolor{black}{Universe of Models}}
                    \annotate[]{below, right}{node4}{\textcolor{black}{Satisfaction Relation}}

                    \vspace{0.25cm}

                    \begin{columns}[t]

                        \column{0.45\textwidth}
                        Example: propositional Horn logic with signature \(\{p, q\}\)

                        \begin{description}
                            \item[\(\llanghorn\):] \(p\), \(\neg{q}\), \(p \rightarrow q\), \(p \land q \rightarrow \bot\), \dots
                            \item[\(\mUniprop\):] \(\{\hmoda, \hmodb, \hmodc, \hmodd\}\)
                            \item[\(\models_\text{Prop}\):] the usual satisfaction relation
                        \end{description}


                        \column{0.45\textwidth}
                        In some systems, some sets are not finitely representable:
                        \begin{align*}
                            \modelsof{\{p \rightarrow q, q\}} &= \{\hmodb, \hmodd\}\\
                            \modelsof{ ? } &= \{\hmodb, \hmodc, \hmodd\}
                        \end{align*}

                    \end{columns}

                \end{block}
            \end{column}

            \separatorcolumn{}

            \begin{column}{\colwidth}
                \begin{block}{4. Finitely Representable Sets}
                    \begin{center}
                        \(\FRsets(\logsys) \coloneqq \{\mSet \subseteq \mUni \mid \text{there is a finite base } \baseb \text{ in } \llang \text{ with } \modelsof{\baseb} = X\}\)
                    \end{center}
                    % \begin{columns}[c]
                    %     \column{0.4\textwidth}
                    %     \hfill \(X \in \FRsets(\logsys)\) iff

                    %     \column{0.6\textwidth}
                    %     \begin{itemize}
                    %         \item \(X \subseteq \mUni\), and
                    %         \item there is a finite base \(\baseb\) in \(\llang\) with \(\modelsof{\baseb} = X\)
                    %     \end{itemize}
                    % \end{columns}
                    \begin{center}
                        \input{fig_linkedcubes_horn_noc.tex} 
                    \end{center}
                \end{block}
            \end{column}

            \separatorcolumn{}
        \end{columns}

        \begin{columns}[t]

            \separatorcolumn{}
            
            \begin{column}{\colwidth}
                \begin{block}{5. Eviction}
                    % \begin{description}[leftmargin=*]
                    %     % \item[\(\FRsel\):] selects a finitely representable set of models and returns a finite base
                    %     \item[Goal:] remove the models in \(\mSet\) from the models of the current base \(\baseb\)
                    %     \item[\(\FRsubs\):] maximal, finitely representable subsets
                    % \end{description}
                    \begin{align*}
                        \eqnmarkbox[mycolor6]{unc}{\modelsof{\mCon_{{\FRsel}}(\baseb, \mSet)}} = 
                        \underbrace{\FRsel(\overbrace{\FRsubs(\underbrace{\eqnmarkbox[mycolor5]{unb}{\eqnmarkbox[mycolor7]{una}{\modelsof{\baseb}} \setminus \mSet}}_{\text{ideal result}}, \logsys)}^{\text{closest options: \textbf{maximal, finitely representable subsets}}})}_{\text{take one of the closest options}}
                    \end{align*}

                    \begin{align*}
                        \modelsof{\mCon(\eqnmarkbox[mycolor7]{node1}{\{p \land q \rightarrow \bot\}}, \{\hmoda\})} &= \FRsel(\FRsubs(\eqnmarkbox[mycolor5]{node2}{\{\hmodb, \hmodc\}})) =\\
                        \FRsel(\{\eqnmarkbox[mycolor6]{node1}{\{\hmodb\}}, \{\hmodc\}\}) &= \modelsof{\{\neg{p}, q \}}
                    \end{align*}

                    \vspace{0.5cm}

                    \begin{center}
                        \input{fig_linkedcubes_horn_subs2.tex}
                    \end{center}
                \end{block}
            \end{column}
            
            \separatorcolumn{}

            \begin{column}{\colwidth}
                \begin{block}{6. Reception}
                    % \begin{description}[leftmargin=*]
                    %     % \item[\(\FRsel\):] selects a finitely representable set of models and returns a finite base
                    %     \item[\(\baseb\):] current base
                    %     \item[\(\mSet\):] models to add
                    %     \item[\(\FRsups\):] minimal, finitely representable supersets
                    % \end{description}
                    \begin{align*}
                        \eqnmarkbox[mycolor6]{unc}{\modelsof{\mExp_{{\FRsel}}(\baseb, \mSet)}} = 
                        \underbrace{\FRsel(\overbrace{\FRsups(\underbrace{\eqnmarkbox[mycolor5]{unb}{\eqnmarkbox[mycolor7]{una}{\modelsof{\baseb}} \cup \mSet}}_{\text{ideal result}}, \logsys)}^{\text{closest options: \textbf{minimal, finitely representable supersets}}})}_{\text{take one of the closest options}}
                    \end{align*}
                    
                    \begin{align*}
                        \modelsof{\mExp(\eqnmarkbox[mycolor7]{node1}{\{p, \neg{q}\}}, \{\hmodb, \hmodd\})} &= \FRsel(\FRsups(\eqnmarkbox[mycolor5]{node2}{\{\hmodb, \hmodc, \hmodd\}})) =\\ \FRsel(\{\eqnmarkbox[mycolor6]{node1}{\mUni}\}) &= \modelsof{\{\}}
                    \end{align*}

                    \vspace{0.5cm}

                    \begin{center}
                        \input{fig_linkedcubes_horn_sups2.tex}
                    \end{center}
                \end{block} 
            \end{column}

            \separatorcolumn{}
            
        \end{columns}

        \begin{columns}[t]
            
            \separatorcolumn{}

            \begin{column}{\colwidth}
                \begin{block}{7. Compatibility}
                    \textbf{Can we always define eviction and reception?}
                    \begin{itemize}
                        \item \(\FRsups\) or \(\FRsubs\) can be empty
                        \item Problem 1: no candidate (e.g.\ Horn logic without \(\bot\))
                        \item Problem 2: a continuum of candidates (no maximal/no minimal)
                    \end{itemize}

                    \vspace{0.5cm}

                    \begin{table}[htb]

                        \setlength\abovecaptionskip{0.05cm}
                        \centering
                        \begin{tabular}{@{}lll@{}}
                            \toprule
                            \multirow{2}{*}{Satisfaction System} & \multicolumn{2}{c}{Compatible} \\ \cmidrule(l){2-3}
                                                                 & \Mconnm{}      & \Mexnm{}      \\ \midrule
                            \(\logsysprop\)                    & \textcolor{oibluishgreen}{Yes}            & \textcolor{oibluishgreen}{Yes}           \\
                            \(\logsyshorn\)                    & \textcolor{oibluishgreen}{Yes}            & \textcolor{oibluishgreen}{Yes}           \\
                            \(\logsyskleene\)                      & \textcolor{oibluishgreen}{Yes}            & \textcolor{oibluishgreen}{Yes}           \\
                            \(\logsyspriest\)                      & \textcolor{oivermillion}{No}            & \textcolor{oibluishgreen}{Yes}           \\
                            \(\logsysfuzzy\)                     & \textcolor{oibluishgreen}{Yes}            & \textcolor{oibluishgreen}{Yes}           \\
                            \(\logsysltlx\)                       & \textcolor{oivermillion}{No}             & \textcolor{oibluishgreen}{Yes}           \\
                            \(\logsysDLABox\)                  & \textcolor{oibluishgreen}{Yes}             & \textcolor{oivermillion}{No}            \\
                            \(\logsysDLLITE\)\textsuperscript{\dagger}                 & \textcolor{oibluishgreen}{Yes}             & \textcolor{oibluishgreen}{Yes}            \\
                            \(\logsysALC\)                  & \textcolor{oivermillion}{No}             & \textcolor{oivermillion}{No}            \\
                            \bottomrule
                        \end{tabular}
                        \caption*{\dagger: with finite signature}
                    \end{table}
                \end{block}    

            \end{column}

            \separatorcolumn{}

            \begin{column}{\colwidth}

                \begin{block}{8. Summary}
                    \begin{itemize}
                        \item New setting in Belief Change: finite representation and models as input
                        \item Definition and characterisation (postulates) of eviction and reception
                        \item Connection between compatibilities and the lattice induced by \(\FRsets(\logsys)\)
                        \item Compatibility in different logics
                    \end{itemize}
                \end{block}

                \begin{block}{9. Open Questions}
                    \begin{itemize}
                        \item Dealing with incompatible satisfaction systems
                        \item What about revision?
                    \end{itemize} 
                \end{block}

                 

                \begin{block}{References}
                    \nocite{Ribeiro2021}
                    \AtNextBibliography{\footnotesize}
                    \printbibliography[heading=none]
                \end{block}
                
            \end{column}

            \separatorcolumn{}

        \end{columns}

    \end{frame}

\end{document}
